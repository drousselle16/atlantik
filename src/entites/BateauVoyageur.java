package entites;
import java.util.List;

public class BateauVoyageur extends Bateau{
    
    private Float                 vitesseBatVoy;
    private String                imageBatVot;
    private List<Equipement>      lesEquipements;

    public BateauVoyageur(Long unId, String unNom,Long uneLongueur,float uneVitesse,Long uneLargeur, String uneImage, List<Equipement> uneCollEquip ){
     super(unId,unNom,uneLongueur,uneLargeur);
     this.vitesseBatVoy = uneVitesse;
     this.imageBatVot = uneImage;
     this.lesEquipements = uneCollEquip;
                
        
    }
    // CONSTRUCTEUR A FAIRE
    
    
    /*
      La méthode toString  doit  retourner par exemple la chaîne suivante ( avec les sauts de ligne )  
      
      Nom du bateau: Luce Isle
      Longueur: 37 mètres 
      Largeur: 9 mètres
      Vitesse: 26 noeuds
      
      Liste des Equipements:
      
             - Accès Handicapé
             - Bar
             - Pont Promenade
             - Salon Vidéo   
     */
        
    @Override
    public String toString() {
       String Ch;
      
       Ch = super.toString();
       Ch= Ch +"Vitesse : "+ vitesseBatVoy + " noeuds \n" + "Liste des équipements du bateau : \n";
       // A FAIRE
       for(Equipement unEquip : lesEquipements){
        Ch = Ch + " - " + unEquip.toString();
                }
       return Ch;    
    }

    //<editor-fold defaultstate="collapsed" desc="Getters et Setters">
   
    public Float getVitesseBatVoy() {
        return vitesseBatVoy;
    }
    
    public void setVitesseBatVoy(Float vitesseBatVoy) {
        this.vitesseBatVoy = vitesseBatVoy;
    }
    
    public String getImageBatVot() {
        return imageBatVot;
    }
    
    public void setImageBatVot(String imageBatVot) {
        this.imageBatVot = imageBatVot;
    }
    
    
    public List<Equipement> getLesEquipements() {
        return lesEquipements;
    }

    public void setLesEquipements(List<Equipement> lesEquipements) {
        this.lesEquipements = lesEquipements;
    }
        
    //</editor-fold>   
} 


